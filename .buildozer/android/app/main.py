#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.7"


#imports do layout:
from kivy.app import App
from kivy.properties import ObjectProperty
from kivymd.navigationdrawer import NavigationLayout
from kivymd.snackbar import Snackbar
from kivymd.theming import ThemeManager
from kivy.uix import image
from kivy.clock import Clock

#Import do MQTT, existem outras bibliotecas que podem ser usadas, como a da adafruit ou mosquitto:
import paho.mqtt.client as mqtt

# Variaveis globais do MQTT:
MQTT_BROKER = "187.191.98.227"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = "1026032/1234/estado"

MQTT_RETORNO = "1026032/1234/retorno"
MQTT_USER = "RI"
MQTT_PASS = "1234"
MQTT_TOPIC_s = "testecont"
mqttc = mqtt.Client(client_id="aplicativo Android", clean_session=True, userdata=None)

#funções e metodos e "interrupções" do protocolo MQTT:
def on_connect(mosq, obj, rc):
    print("Conectado ao Broker")
    mqttc.subscribe(MQTT_RETORNO,0)

def on_publish(client, userdata, mid):
    print("Mensagem publicada")

def on_message(mosq, obj, msg):
    print("Mensagem Recebida")
    print(msg.payload)
    atividades.mostra("Recebido")

def on_subscribe(mosq,obj,mid,granted_qos):
    print("Subscrito")
    mqttc.loop_start()

def manda(msg):
    try:
        mqttc.reconnect()
        mqttc.publish(MQTT_TOPIC, msg)
        print("Comando: " + msg)
        return True
    except():
        return False


#Apontando as tags para as devidas funções:
mqttc.username_pw_set(MQTT_USER, MQTT_PASS)
mqttc.on_publish = on_publish
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.on_subscribe = on_subscribe

mqttc.connect(MQTT_BROKER, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)
mqttc.loop_start()

#Classe integrada ao layout do app, por esta classe podemos fazer a cominucação entre o Kivy e o Python (front e backend)
class atividades(NavigationLayout):
#Ao precionar o botão ele aciona o relé (haverá mais parâmetros para diferentes relés):
    def rele(self):
        manda("iB")
        if manda("p200"):
            Snackbar(text="Enviado").show()
        else:
            Snackbar(text="Houve algum erro, verifique sua conexão")

#Dispara uma notificação estilo popup na parte inferior com o texto enviado em 'data':
    def mostra(data):
        Snackbar(text=data).show()


#Define os parâmetros e linka o tela.kv ao resto do projeto:
class TelaApp(App):
    theme_cls = ThemeManager()
    theme_cls.theme_style = 'Dark'
    theme_cls.primary_palette = 'LightGreen'
    previous_date = ObjectProperty()
    title = "Rele Wifi"

#inicia a tela
TelaApp().run()
