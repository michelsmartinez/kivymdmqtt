Projeto que engloba a criação de um APP (Android, IOS(precisa de um MAC para gerar), Windows e Linux)
Este App utiliza além do Kivy um projeto chamado Kivy MD que adiciona formas, animações e cores no padrão Android;
O App tem seu back em Python 3, utilizando a biblioteca anterior para gerar a interface;
Para comunicação com um servidor ou hardware (como foi o caso deste teste) foi utilizado MQTT, mas poderia ser 
utilizado facilmente sockets, Firebase, ou outro protocolo de comunicação.
Para gerar o APP para Android foi utilizado o buildozer.
